from .app import manager,db, login_manager, app
from sqlalchemy import Table, Column, Integer, ForeignKey, or_, and_
from sqlalchemy.orm import relationship, backref
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.inspection import inspect
from sqlalchemy import PrimaryKeyConstraint
from sqlalchemy.sql.expression import func
from hashlib import sha256
from werkzeug import secure_filename
from flask import jsonify
import os
#~ from flask.ext.principal import Permission # identity_loaded, , RoleNeed, \     UserNeed
import time
import json

from flask.ext.login import UserMixin

Link_Tag_Record= Table('tag_record_link', db.Model.metadata,
    Column('record_id', Integer, ForeignKey('record.id')),
    Column('tag_id', Integer, ForeignKey('tag.id'))
)


class User(db.Model, UserMixin):
	__tablename__="user"
	id = db.Column(db.Integer,primary_key=True)
	username=db.Column(db.String(50))
	password = db.Column(db.String(256))

	def __repr__(self):
		return "User< n"+str(self.id)+", "+self.username+" >"



class Visibility(db.Model):
	__tablename__="visibility"
	id=db.Column(db.Integer,primary_key=True)
	name = db.Column(db.String(20))

	def __repr__(self):
		return "Visibility< n"+str(self.id)+", "+self.name+" >"

class Record(db.Model):
	__tablename__="record"
	id=db.Column(db.Integer,primary_key=True)
	name= db.Column(db.String(100))
	owner = db.Column(db.Integer, db.ForeignKey("user.id"))
	visibility = db.Column(db.Integer, db.ForeignKey("visibility.id"))
	length=db.Column(db.Integer)
	tags = db.relationship("Tag",secondary=Link_Tag_Record, backref="parents")
	date = db.Column(db.Integer, db.ForeignKey("date.id"))
	lienBrut = db.Column(db.String(100))
	lienTraite = db.Column(db.String(100))
	lienMusiqueGen = db.Column(db.String(100))
	lienMusiqueModif = db.Column(db.String(100))

	def __repr__(self):
		TagsStr = "[ "
		for tag in self.tags:
			TagsStr+= str(tag)+ " "
		TagsStr+=" ]"
		res = "Record< n"+str(self.id)+", "+self.name+", UserId="+str(self.owner)+", VisiblId="+str(self.visibility)+", len="+str(self.length)+ ", tags= "+TagsStr+", dateId="+str(self.date)
		return res+", raw="+str(self.lienBrut)+", traite="+str(self.lienTraite)+", musiqueGenere="+str(self.lienMusiqueGen)+", musiqueModif="+str(self.lienMusiqueModif)+" >\n"

	def to_json(self):
		test=[tag.name for tag in self.tags]
		json_quest={
			'id' : self.id,
			'name' : self.name,
			'visibility': self.visibility,
			'tags': test
			}
		return json_quest

	def removeTag(self, tag):
		self.tags.remove(tag)

	def addTag(self, tag):
		self.tags.append(tag)

	def setVisibility(self, visibility):
		self.visibility = visibility.id

	def setName(self, newName):
		ancienNom = self.name
		for mot in ancienNom.split():
			tag = get_Element(Tag, Tag.name, mot)
			if tag !=None:
				self.removeTag(tag)
		self.name = newName
		for mot in newName.split():
			tag = get_Element(Tag, Tag.name, mot)
			if  tag == None:
				tag = newTag(mot)
			self.addTag(tag)

	def setMusiqueGenere(self, lien):
		self.lienMusiqueGen = lien

	def removeMusiqueGenere(self):
		self.lienMusiqueGen = None

	def setMusiqueModif(self, lien):
		self.lienMusiqueGen = lien

	def removeMusiqueModif(self):
		self.lienMusiqueModif = None

class Tag(db.Model):
	__tablename__="tag"
	id=db.Column(db.Integer,primary_key=True)
	name=db.Column(db.String(100))

	def __repr__(self):
		return "Tag"


class Date(db.Model):
	__tablename__ = 'date'
	id = Column(db.Integer, primary_key=True)
	date = Column(db.Integer)

	def __repr__(self):
		return "Date< n"+str(self.id)+", "+str(self.date)+" >"



#_____________________________1.Fonctions Generals________________________________________


def Element_exist(nomTable, nomTableparametre, name):
	"""retourn vrais si l'element rechercher exist, faux sinon"""
	return get_Element(nomTable, nomTableparametre, name) != None

def get_ElementsById(nomTable, id):
	"""retourn l'element correspondant à l'id, sinon renvoie None"""
	return nomTable.query.get(id)

def get_Element(nomTable, nomTableparametre, name):
	"""retourn le premier element correspondant à la recherche"""
	try:
		return nomTable.query.filter(nomTableparametre== name)[0]
	except:
		return None


def get_Elements(nomTable):
	"""retourn la liste des elements d'une table"""
	try:
		return nomTable.query.filter().all()
	except:
		return []


def get_IdDispo(nomTable):
	res = 0
	malist = get_Elements(nomTable)
	if malist != []:
		for elem in malist:
			if elem.id> res:
				res = elem.id
		return res+1
	return res



#_____________________________2.Fonctions sur la table Utilisateurs________________________________________

@login_manager.user_loader
def get_UserById(id):
	"""retourne l'utilisateur corespondant a l'id souhaiter"""
	return get_ElementsById(User, id)

def Username_exist(name):
	"""retourne vrais si l'username est deja pris"""
	return get_Element(User, User.username,name) != None


def get_User(username, password):
	"""retourne l'utilisateur correspondant a (username, password) """
	user=get_Element(User,User.username, username)
	if user is None:
		return None
	m=sha256()
	m.update(password.encode())
	passwd=m.hexdigest()
	return user if passwd==user.password else None

def get_RecordUser(userid):
	return Record.query.filter(Record.owner== userid).all()

def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1] in app.config['ALLOWED_EXTENSIONS']

def get_UserAccesList(userid):
	"""retourn la liste des enregistrement auquel l'utilisateur à acces"""
	if userid!= None:
		return Record.query.filter(or_(Record.owner ==userid, Record.visibility ==1)).distinct().all()
	return Record.query.filter(Record.visibility ==1).all()


def have_UserAccesTo(userId, recordid):
	"""retourn vrais si l'utilisateur corespondant a l'id a acces au record correspondant au recordid, faux sinon"""
	if userId != None:
		return get_Element(Record, Record.id, recordid) in get_UserAccesList(userId)
	else:
		return get_Element(Record, Record.id, recordid) in Record.query.filter( Record.visibility ==1).all()


#_____________________________3.Fonctions de recherche________________________________________

# le reserche de bas, pas d'utilisateur connecter
def reserchLike(nomTable, nomTableColumn, nomElement):
	"""retourne la liste des elements resemblant a ce qui est souhaiter"""
	return nomTable.query.filter(and_( Record.visibility ==1,nomTableColumn.like("%"+nomElement+"%"))).all()

def reserchLikeCo(nomTable, nomTableColumn, nomElement, idUser):
	"""retourne la liste des elements resemblant a ce qui est souhaiter"""
	return nomTable.query.filter(and_(or_( Record.visibility ==1,Record.owner ==idUser),nomTableColumn.like("%"+nomElement+"%"))).distinct().all()

#non utiliser pour l'instant
def appartienA(element1 , liste):
	""""retourne vrais si l'element est dans la liste"""
	return element1 in liste

# recherche que sur les elements public
def rechercheToutTitre(phrase):
	"""retourne deux dictionnaire: un {idRecord: pertinence}, {idRecord: record}"""
	dicoId = {}
	dicoRecord = {}
	res = []
	listMots =phrase.split()
	for mot in listMots:
		listElements = reserchLike(Record, Record.name, mot)
		for element in listElements:
			if element.id not in dicoId.keys():
				dicoId[element.id] = 1
				dicoRecord[element.id] = element
			else:
				dicoId[element.id] +=1
	return dicoId, dicoRecord

# recherche  sur les elements public et ceux de l'utilisateur
def rechercheToutTitreCo(phrase, UserId):
	"""retourne deux dictionnaire: un {idRecord: pertinence}, {idRecord: record}"""
	dicoId = {}
	dicoRecord = {}
	res = []
	listMots =phrase.split()
	for mot in listMots:
		listElements = reserchLikeCo(Record, Record.name, mot, UserId)
		for element in listElements:
			if element.id not in dicoId.keys():
				dicoId[element.id] = 1
				dicoRecord[element.id] = element
			else:
				dicoId[element.id] +=1
	return dicoId, dicoRecord

# recherche que sur les elements public
def rechercheToutTag(phrase):
	"""retourne deux dictionnaire:  {idRecord: pertinence}, {idRecord: record}"""
	dicoId = {}
	dicoRecord = {}
	Records = get_UserAccesList(None)
	res = []
	listMots =phrase.split()
	for mot in listMots:
		listTag = reserchLike(Tag, Tag.name, mot)
		for tag in listTag:
			for record in Records:
				if tag in record.tags:
					if record.id not in dicoId.keys():
						dicoId[record.id] = 1
						dicoRecord[record.id] = record
					else:
						dicoId[record.id] +=1
	return dicoId, dicoRecord

# recherche  sur les elements public et ceux de l'utilisateur
def rechercheToutTagCo(phrase, UserId):
	"""retourne deux dictionnaire: un {idRecord: pertinence}, {idRecord: record}"""
	dicoId = {}
	dicoRecord = {}
	Records = get_UserAccesList(UserId)
	res = []
	listMots =phrase.split( )
	for mot in listMots:
		listTag = reserchLike(Tag, Tag.name, mot)
		for tag in listTag:
			for record in Records:
				if tag in record.tags:
					if record.id not in dicoId.keys():
						dicoId[record.id] = 1
						dicoRecord[record.id] = record
					else:
						dicoId[record.id] +=1
	return dicoId, dicoRecord



# recherche pour date < X ou mach dans la phrase
def rechercheDateInfOu(phrase, date, fonction ):
	"""retourne deux dictionnaire: un {idRecord: pertinence}, {idRecord: record}"""
	dicoId, dicoRecord = fonction(phrase)
	dateRef = extractionDate(date)
	listDateCorsp = Date.query.filter(Date.date < dateRef).all()
	for date in listDateCorsp:
		records = Record.query.filter( Record.date == date.id).all()
		for record in records:
			if record.id not in dicoId.keys():
				dicoId[record.id] = 1
				dicoRecord[record.id] = record
			else:
				dicoId[record.id] +=1
	return dicoId, dicoRecord

# recherche pour date < X et mach dans la phrase
def rechercheDateInfEt(phrase, date, fonction ):
	"""retourne deux dictionnaire: un {idRecord: pertinence}, {idRecord: record}"""
	dicoId, dicoRecord = fonction(phrase)
	dateRef = extractionDate(date)

	dicoIdRes = {}
	dicoRecordRes = {}
	listDateCorsp = Date.query.filter(Date.date < dateRef).all()
	for date in listDateCorsp:
		records = Record.query.filter( Record.date == date.id).all()
		for record in records:
			if record.id  in dicoId.keys():
				dicoRecordRes[record.id] = record
				dicoIdRes[record.id] = dicoId[record.id] +1
	return dicoIdRes, dicoRecordRes

def rechercheDateEqOu(phrase, date, fonction):
	dicoId, dicoRecord = fonction(phrase)
	dateRef = extractionDate(date)
	listDateCorsp = Date.query.filter(Date.date == dateRef).all()
	for date in listDateCorsp:
		records = Record.query.filter( Record.date == date.id).all()
		for record in records:
			if record.id not in dicoId.keys():
				dicoId[record.id] = 1
				dicoRecord[record.id] = record
			else:
				dicoId[record.id] +=1
	return dicoId, dicoRecord

def rechercheDateEqEt(phrase, date, fonction ):
	dicoId, dicoRecord = fonction(phrase)
	dateRefInf = extractionDate(date)
	dateRefSup = extractionDate(date)+86400
	dicoIdRes = {}
	dicoRecordRes = {}
	listDateCorsp = Date.query.filter(and_(Date.date >= dateRefInf,Date.date < dateRefSup )).all()
	for date in listDateCorsp:
		records = Record.query.filter( Record.date == date.id).all()
		for record in records:
			if record.id in dicoId.keys():
				dicoRecordRes[record.id] = record
				dicoIdRes[record.id] = dicoId[record.id] +1
	return dicoIdRes, dicoRecordRes

def rechercheDateSupOU(phrase, date, fonction):
	dicoId, dicoRecord = fonction(phrase)
	dateRef = extractionDate(date)
	listDateCorsp = Date.query.filter(Date.date > dateRef).all()
	for date in listDateCorsp:
		records = Record.query.filter( Record.date == date.id).all()
		for record in records:
			if record.id not in dicoId.keys():
				dicoId[record.id] = 1
				dicoRecord[record.id] = record
			else:
				dicoId[record.id] +=1
	return dicoId, dicoRecord

def rechercheDateSupEt(phrase, date, fonction ):
	dicoId, dicoRecord = fonction(phrase)
	dateRef = extractionDate(date)

	dicoIdRes = {}
	dicoRecordRes = {}
	listDateCorsp = Date.query.filter(Date.date > dateRef).all()
	for date in listDateCorsp:
		records = Record.query.filter( Record.date == date.id).all()
		for record in records:
			if record.id  in dicoId.keys():
				dicoRecordRes[record.id] = record
				dicoIdRes[record.id] = dicoId[record.id] +1
	return dicoIdRes, dicoRecordRes

# permet d'obtenir la date (sansHH:MM:SS
def extractionDate(date):
	date = time.strftime("%d %m %Y", time.localtime(date))
	dateRef = int(time.mktime(time.strptime(date, "%d %m %Y")))- time.timezone
	return dateRef

# a appeler pour avoir la list des tuple recherche double et non co
def conversionListTuple_4args(phrase, date, fonctionTitre_ou_Tag, recherchetypeDate):
	res = []
	dicoId, dicoRecord = recherchetypeDate(phrase, date, fonctionTitre_ou_Tag)
	tuplesTrie = sorted(dicoId.items(), key=lambda x: x[1], reverse=True)
	for identifiant,nbOccurence in tuplesTrie:
		res.append( (identifiant, dicoRecord[identifiant].name))
	return res

# a appeler pour avoir la list des tuple recherche double et  co
#~ def conversionListTuple_5args(phrase, date, fonctionTitre_ou_Tag, recherchetypeDate, userId):
	#~ res = []
	#~ dicoId, dicoRecord = recherchetypeDate(phrase, date, fonctionTitre_ou_Tag)
	#~ tuplesTrie = sorted(dicoId.items(), key=lambda x: x[1], reverse=True)
	#~ for identifiant,nbOccurence in tuplesTrie:
		#~ res.append( (identifiant, dicoRecord[identifiant].name))
	#~ return res

def conversionListTuple_RechercheSimpleDeco(phrase, fonction):
	# fonctionnne pour la recherche de titre ou de tag
	res = []
	dicoId, dicoRecord = fonction(phrase)
	tuplesTrie = sorted(dicoId.items(), key=lambda x: x[1], reverse=True)
	for identifiant,nbOccurence in tuplesTrie:
		res.append( (identifiant, dicoRecord[identifiant].name))
	return res

def conversionListTuple_RechercheSimpleCo(phrase, fonction, userId):
	res = []
	dicoId, dicoRecord = fonction(phrase, userId)
	tuplesTrie = sorted(dicoId.items(), key=lambda x: x[1], reverse=True)
	for identifiant,nbOccurence in tuplesTrie:
		res.append( (identifiant, dicoRecord[identifiant].name))
	return res

#_____________________________4.Fonctions de creation d'element________________________________________ create new
def newTag(mot):
	""" cree un nouveaux tag s'il n'est pas déjà dans la base de donnee et le renvoie"""
	db.create_all()
	listTag = get_Elements(Tag)
	if not Element_exist(Tag, Tag.name,mot):
		t = Tag(name=mot)
		db.session.add(t)
		db.session.commit()
		return t
	else:
		return None


def newUser(username, password):
	from hashlib import sha256
	db.create_all()
	m=sha256()
	m.update(password.encode())
	elem = username
	elementExist = Element_exist(User, User.username,elem)
	while elementExist:
		elem = input("Entrez un autre pseudonyme: ")
		elementExist = Element_exist(User, User.username,elem)
	u=User(username=elem, password=m.hexdigest())
	db.session.add(u)
	db.session.commit()

# Attention aucune verification n'est effectuer pour l'ajout!!! je m'en occupe se week-end
def newRecord( title, OwnerId,visibilitieId,  tagsList, thedate, linkBrut, linkTraite):
	db.create_all()
	record = Record( name= title,owner=OwnerId, visibility=visibilitieId, length=0, tags=[], date=thedate, lienBrut= linkBrut, lienTraite=linkTraite)
	db.session.add(record)
	db.session.commit()


def newVisibility(newName):
	db.create_all()
	visibility = Visibility(name=newName)
	db.session.add(visibility)
	db.session.commit()


# SUREMENT A SUPP
def getuser(username):
	try:
		return get_Element(User, User.username,username).id
	except:
		return None
