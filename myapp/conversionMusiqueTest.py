from __future__ import division
import collections
import itertools
from random import choice
from pyknon.genmidi import Midi
from pyknon.music import Note, NoteSeq, Rest
from pyknon.MidiFile import MIDIFile
import csv, random

def extraction(nomFichier):
    """Extrait les valeurs du fichier csv et retourn une liste contenant les valeurs correspondant à l'attention et la meditation"""
    fileCsv = open(nomFichier, 'r')
    with fileCsv as csvfile:
        reader = csv.DictReader(csvfile)
        Attentions = []
        Meditation = []
        for row in reader:
            if(row['attentionValue'] !=None):
                Attentions.append(int(row['attentionValue']))
                Meditation.append(int(row['meditationValue']))
            else:
                break
    return [Attentions,Meditation]

def conversion(ArrayListValeur, maximum,maxValeur ):
    """Modifie les valeurs d'une liste passer en paramettre pour les faire entrer dans l'interval [0, maxValeur["""
    discriminant = maximum//maxValeur
    resultat = []
    for valeur in ArrayListValeur:
        resultat.append(valeur//discriminant)
    return resultat



def creationNoteList(listeValeurs):
    """transforme une liste d'integer en liste de note"""
    resultat = []
    listeValeurs = conversion(listeValeurs, 110, 11)
    #listeOctave = conversion(listeValeurs, 12)
    #listVolume = conversion(listeValeurs, 127)
    for i in range(len(listeValeurs)):
            dur = 0.25
            volume = random.randint(100,127)
            #octave = random.randint(1,12)
            resultat.append(Note(value=listeValeurs[i],volume=volume, dur=dur))
    return resultat

def harmonizationMaison(listeNotes, degreHarmonisation):
    """Permet d'armoniser un ensemble de note en crean x note intermediare entre chaque (x = degreHarmonisation )"""
    resultat = []
    for i in range(len(listeNotes)-1):
        if(listeNotes[i].value != listeNotes[i+1].value):
            resultat.append(listeNotes[i])
        for j in range(1,degreHarmonisation):
            if(abs(listeNotes[i].value- listeNotes[i+1].value) >2):
                resultat.append(Note(value=(listeNotes[i].value*(degreHarmonisation-j)+listeNotes[i+1].value*j )//(degreHarmonisation),
                    volume=random.randint(100,160)))
    resultat.append(listeNotes[-1])
    return resultat


def creationNoteSeq(listeNotes):
    return NoteSeq(listeNotes)


def moyenneListe(liteEntier):
    res = 0
    nb = 0
    for element in liteEntier:
        res +=element
        nb +=1
    return res//nb

def calculTempo(listeListeValeurs):
    """ cette fonction ne compare qu'attention et mediation et renvoie un rythme plus ou moins rapide en fonctions de l'attention"""
    tempo = 120
    # On changera le tempo toutes les 20notes
    nbTempo =  len(listeListeValeurs[0])//20
    tempos = []
    for i in range(nbTempo):
        dif = moyenneListe(listeListeValeurs[0][i*20:(i+1)*20]) - moyenneListe(listeListeValeurs[1][i*20:(i+1)*20])
        tempos.append(tempo+dif)
    print(tempos)
    return tempos

def ajoutRest(listeNotes):
    res = []
    for i in range(len(listeNotes)):
        res.extend([listeNotes[i], Rest(0.25)])
    return res

def creationMidi(fileName,listeListeValeurs, _instrument, nbRep=1, duree=[0.666,2]):
    """ cree un fichier midi a partie de [[Valeurs],[Valeurs]],
                                        une liste d'instrument ou un instrument,
                                        le nombre de répétition,
                                        les temps de pose par listeValeurs """
    for nb in range(nbRep):
        for i in range(len(listeListeValeurs)):
            listeListeValeurs[i].extend(listeListeValeurs[i])
    tempoR = calculTempo(listeListeValeurs)
    midi = MIDIFile(len(listeListeValeurs))

    #seqNotes = creationNoteSeq(listPartition).stretch_dur(duree[0])
    for i in range(len(listeListeValeurs)):
        listPartition =harmonizationMaison(creationNoteList(listeListeValeurs[i]),5)
        for note in listPartition:
            midi.addNote(i, note.octave, note.value, i, note.dur, note.volume)
        for tempo in tempoR:
            midi.addTempo(i, i*6, tempoR[i])
        midi.tracks[i].closeTrack()
    midi.close()
    with open(fileName, 'wb') as midifile:
        midi.writeFile(midifile)

# creationMidi("test01.mid",extraction('../data.csv'))
# creationMidi("test02.mid",extraction('../courbes10.csv'))
# creationMidi("test03.mid",extraction('../courbes11.csv'))





#creationMidi("testMidiFile.mid",extraction('../courbes19.csv'), [0, 49], 2 )
