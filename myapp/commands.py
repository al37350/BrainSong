

import yaml
from hashlib import sha256
from .models import User,Record, Visibility, Date, Tag
from .models import Element_exist, get_Elements, get_ElementsById, get_Element, Username_exist, newUser, newTag
from .app import manager, db
# supp appel a verfiVisibilite
@manager.command
def loaddb(filenameRecords, filenameUsers):

	db.create_all()
	loadUserFrom(filenameUsers)
	#utilisateur bien creer


	v1 = Visibility(name="publique")
	v2 = Visibility(name="privé")
	db.session.add(v1)
	db.session.add(v2)
	db.session.commit()
	#visibilitée bien créer

	ToutlesTags= {}
	TouteslesDates= {}
	TitleConcatList = []
	recordsFromFile = yaml.load(open(filenameRecords, encoding="utf8"))


	#1er tours, on cree les tags , dates
	for record in recordsFromFile:
		tagList = record['tags']

		thedate =record['date']

		for tag in tagList:
			t = newTag(str(tag))
			if t != None:
				ToutlesTags[t.name]= t

		if (thedate!=None and thedate not in TouteslesDates):
			laDate = Date( date=thedate)
			db.session.add(laDate)
			TouteslesDates[laDate.date] = laDate.id

	db.session.commit()

	#~ #2eme tours, on cree les enregistrement

	for record in recordsFromFile:
		title = record['recordTitle']
		visibilitie = get_Element(Visibility, Visibility.name, record['visibility']).id

		if Username_exist(record['user']):
			OwnerId =get_Element(User, User.username,record['user']).id

			linkBrut = record['lienBrut']
			linkTraite = record['lienTraite']
			tagList = record['tags']
			listTagAssocie = [ToutlesTags[str(tag)] for tag in tagList ]
			thedate = get_Element(Date,Date.date, record['date']).id
			FileConcat = Concaten(str(OwnerId), title, thedate, " .")
			if (title !=None) and FileConcat not in TitleConcatList:
				record = Record( name= title,owner=OwnerId, visibility=visibilitie, length=0, tags=listTagAssocie, date=thedate, lienBrut= linkBrut, lienTraite=linkTraite)
				db.session.add(record)
				#~ print("record bien creer et ajouter")
				TitleConcatList.append(FileConcat)

	db.session.commit()



def Concaten(OwnerName, recordtitle, date, toSupr):
	phrase = OwnerName + recordtitle + str(date)
	for elem in toSupr:
		phrase = phrase.replace(elem, "")

	return phrase.lower()

@manager.command
def loadUserFrom(filenameUsers):
	usersFromFile = yaml.load(open(filenameUsers, encoding="utf8"))
	myusers = []
	for person in usersFromFile:
		UserName = person['userName']
		passWd = person['password']
		newUser(UserName, passWd )



@manager.command
def syncdb():
	db.create_all()
