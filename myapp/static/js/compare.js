function compareGraph(listeid, listename) {
    var options = {
        lang: {
            months: ["Janvier "," Février ","Mars "," Avril "," Mai "," Juin "," Juillet "," Août "," Septembre ",
            " Octobre "," Novembre "," Décembre"],
            weekdays: ["Dim "," Lun "," Mar "," Mer "," Jeu "," Ven "," Sam"],
            shortMonths: ['Jan', 'Fev', 'Mar', 'Avr', 'Mai', 'Juin', 'Juil','Août', 'Sept', 'Oct', 'Nov', 'Déc'],
            decimalPoint: ',',
            resetZoom: 'Reset zoom',
            resetZoomTitle: 'Reset zoom à 1:1',
            downloadPNG: "Télécharger au format PNG image",
            downloadJPEG: "Télécharger au format JPEG image",
            downloadPDF: "Télécharger au format PDF document",
            downloadSVG: "Télécharger au format SVG vector image",
            exportButtonTitle: "Exporter image ou document",
            printChart: "Imprimer le graphique",
            loading: "Laden..."
        },
        chart: {
            renderTo: 'container',
            zoomType: 'x',
            type: 'line',
            marginRight: 10,
            marginBottom: 100,
            plotBorderColor: '#346691',
            plotBorderWidth: 1,
            panning: true,
            panKey:'shift'
        },
        title: {
            text: 'Comparaison'
        },
        xAxis: {
            categories:[],
            title:{
               text: 'Temps' , 
               gridLineWidth: 1
            }
        },
        yAxis: {
            title: {
            text: 'Valeurs' ,
            gridLineWidth: 1
            },

            plotLines: [{
            value: 0,
            width: 1,
            color: '#FF0000'
            }]
        },
        tooltip: {
            borderColor: '#4b85b7',
            backgroundColor: '#edf1c8',
            formatter: function () {
                return this.series.name+' de <b>'+this.y+'</b> au temps <b>'+this.x+'</b>';
            }
        },
        plotOptions: {
            series: {
                marker: {
                    enabled: false
                }
            }
        },
        credits: {
            text: 'BrainSong',
            href: "{{url_for('home')}}"
        },
    };
    series = [];
    options.legend={};
    for (var id in listeid) {
        function f(id){
            $.ajaxSetup({async:false});
            $.get('courbes'+listeid[id]+'.csv', function(data) {
                // Split the lines
                var lines = data.split('\n');
                series.push({data:[]},{data:[]});
                series[id*2].name = "Attention de \""+listename[id]+"\"";
                series[id*2+1].name = "Médidation de \""+listename[id]+"\"";
                $.each(lines, function(lineNo, line) {
                    var items = line.split(',');
                    // header line containes categories
                    if (lineNo != 0 && id==0) {
                        $.each(items, function(itemNo, item) {
                            if (itemNo==0) options.xAxis.categories.push(item);
                        });
                    }

                    // the rest of the lines contain data with their name in the first position
                    $.each(items, function(itemNo, item) {
                        if(lineNo!=0) {
                            if (itemNo==1) {series[id*2].data.push(parseInt(item));}
                            else if (itemNo==2) {series[id*2+1].data.push(parseInt(item));}
                        }
                    });
                });
            });
        };
        f(id);
    };
    options.series=series;
    var chart = new Highcharts.Chart(options);
}