from flask import Flask
import os
app= Flask(__name__)
app.debug=True
app.config['SECRET_KEY']="c6922300-2d1b-40bb-a21c-0535374c02a6"
app.config['SQLALCHEMY_TRACK_MODIFICATIONS']=True

UPLOAD_FOLDER=os.getcwd()+'/myapp/uploads/'
ALLOWED_EXTENSIONS=set(['csv', 'wav'])
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
app.config['ALLOWED_EXTENSIONS'] = ALLOWED_EXTENSIONS

from flask.ext.mail import Mail
app.config.update(
	DEBUG=True,
	MAIL_SERVER='smtp.univ-orleans.fr',
	MAIL_PORT=25,
	MAIL_DEFAULT_SENDER='brainsong.serveur@gmail.com',
	)
mail = Mail(app)

from flask.ext.script import Manager, Server
server = Server(host="0.0.0.0", port=5000)
manager=Manager(app)
manager.add_command("runserver", server)

import os.path
def mkpath(p):
	return os.path.normpath(
		os.path.join(
			os.path.dirname(__file__),
			p))

from flask.ext.sqlalchemy import SQLAlchemy
app.config['SQLALCHEMY_DATABASE_URI']=(
	'sqlite:///'+mkpath('../myapp.db'))
db=SQLAlchemy(app)

from flask.ext.login import LoginManager
login_manager = LoginManager(app)
